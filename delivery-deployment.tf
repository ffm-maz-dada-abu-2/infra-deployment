resource "kubernetes_deployment_v1" "delivery" {
  metadata {
    name = "delivery"
    labels = {
      test = "delivery"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "delivery"
      }
    }

    template {
      metadata {
        labels = {
          test = "delivery"
        }
      }

      spec {
        container {
          image = "921728388910.dkr.ecr.us-east-1.amazonaws.com/ffm-delivery-service-1:latest"
          name  = "delivery"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "delivery_svc" {
  metadata {
    name = "delivery"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.delivery.metadata.0.labels.test
    }
    
    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}
